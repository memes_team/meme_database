import sys
import os
import shutil
import logs
sys.path.append('/path/to/meme_database')
from PyQt5 import QtWidgets
from PyQt5.Qt import *
from PyQt5 import QtGui
from Interface.ui_main import Ui_MainWindow
from Interface.ui_functions import *
from Interface import ui_functions
from Interface import files_rc
from Interface.ui_tiles import DropArea, Meme_Overview_Frame, CopyButton
from Interface import ui_sidepanel
from Interface import ui_meme
from Database import dbcontrol
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore
from Interface import ui_settings


icon = "./Interface/Icons/memes_icon.ico"


def start_app():
    """Запуск приложения"""
    if len(sys.argv) > 1:
        # Перенос файлов на иконку
        files = sys.argv[1:]
        count = 0

        directory = ui_settings.settings_data["memefolder"]

        for i in files:
            try:
                if os.path.exists(i) and not os.path.isdir(i):
                    copy_files = os.listdir(directory)

                    if (os.path.abspath(os.path.dirname(i)) == directory or
                            os.path.basename(i) in copy_files):
                        continue

                    count += 1
                    new_file = shutil.copy(i, directory).replace(directory, "")

                    dbcontrol.from_folder_to_db(new_file)

                    dbcontrol.conn.commit()
            except BaseException as error:
                logs.get_log(error)

    else:
        # Непосредственно запуск программы
        argv = sys.argv.copy()
        argv.append("Appname")
        argv.append("--platform")
        argv.append("windows:dpiawareness=0")

        app = QtWidgets.QApplication(argv)
        app.setAttribute(Qt.AA_DisableHighDpiScaling, True)
        app.setWindowIcon(QIcon(icon))
        window = MainWindow()
        window.setWindowIcon(QIcon(icon))
        sys.exit(app.exec_())


class MainWindow(QMainWindow):
    """Главное окно приложения"""
    resized = QtCore.pyqtSignal()

    def __init__(self):
        """Инициализация окна"""
        QMainWindow.__init__(self)

        self.wi = self.size().width()
        self.he = self.size().height()

        self.directory = ui_settings.settings_data["memefolder"]
        if not os.path.isdir(self.directory):
            os.mkdir(self.directory)

        self.tile_size = ui_settings.settings_data["memesize"]
        self.container = []

        self.resized.connect(self.edit_size)

        self.ui = Ui_MainWindow()

        self.to_copy = None
        self.search_filter = ""

        self.ui.setupUi(self)
        self.ui.sorting_combo_box.activated.connect(self.sorting_box_click)

        self.panel_status = False
        self.init_ui_tiles()
        self.sidepanel = ui_sidepanel.SidePanel(self)
        self.sidepanel.buttonFunctionality(self.ui.btn_tags)

        self.meme_frame = ui_meme.Meme_Frame(self)

        self.settings = ui_settings.Settings_Frame(self)
        self.settings.buttonFunctionality(self.ui.btn_settings)

        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(QIcon(icon))

        show_action = QAction("Развернуть", self)
        hide_action = QAction("Свернуть", self)
        quit_action = QAction("Выход", self)
        show_action.triggered.connect(self.show)
        hide_action.triggered.connect(self.hide)
        quit_action.triggered.connect(qApp.quit)
        tray_menu = QMenu()
        tray_menu.addAction(show_action)
        tray_menu.addAction(hide_action)
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()
        self.tray_icon.activated.connect(self.tray_click)

        self.ui.frame_title.mouseMoveEvent = self.moveWindow
        UIFunctions.uiDefinitions(self)
        self.show()

    def create_labels(self):
        """Создание плиток с изображениями"""
        self.container.clear()
        for i, file_name in enumerate(dbcontrol.meme_list):
            should_draw = True

            if file_name in dbcontrol.meme_desc and dbcontrol.meme_desc[file_name].lower().find(self.search_filter.lower()) == -1:
                should_draw = False

            for name, state in ui_sidepanel.TagState.items():
                if name not in dbcontrol.meme_tags[file_name] and state == 1:
                    should_draw = False

            for name, state in ui_sidepanel.TagState.items():
                if name in dbcontrol.meme_tags[file_name] and state == 2:
                    should_draw = False

            if should_draw:
                frame = Meme_Overview_Frame(self, self.directory, file_name)
                frame.resize(self.tile_size, self.tile_size)

                self.container.append(frame)

        self.edit_size()

    def edit_size(self):
        """Изменение размера области с изображениями"""
        self.wi = self.size().width()
        self.he = self.size().height()

        self.scroll_area.resize(self.wi - 72 - 248 * self.panel_status,
                                self.he - 113)

        xmax = self.scroll_area.width() // (self.tile_size + 10)
        ymax = 0
        while ymax * self.xmax < len(self.container):
            ymax += 1

        if xmax != self.xmax:
            self.xmax = xmax
            self.tiles()

    def reinit(self):
        """Повторная отрисовка изображений"""
        self.layout.removeWidget(self.w)
        self.w.deleteLater()
        self.w = None
        self.layout = QGridLayout()
        self.layout.setContentsMargins(3, 3, 0, 3)
        self.layout.setSpacing(5)

        self.w = QWidget()
        self.w.setLayout(self.layout)
        self.scroll_area.setWidget(self.w)

        self.xmax = self.scroll_area.width() // (self.tile_size + 10)

        self.xmax -= 1
        self.create_labels()

    def init_ui_tiles(self):
        """Создание области с изображениями мемов"""
        self.scroll_area = DropArea(self)
        self.scroll_area.move(32, 95)
        self.scroll_area.resize(self.wi - 72, self.he - 113)
        self.scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidgetResizable(True)

        self.scroll_area.setStyleSheet("""
            QScrollBar:vertical {
                 background: rgb(231, 231, 231);
             }
             QScrollBar::handle:vertical {
                 background: rgb(160, 160, 160);
                 border-radius: 4px;
                 margin-left: 3px;
                 margin-right: 2px;
                 min-height: 20px;
             }
             QScrollBar::handle:vertical:hover {
                 background: rgb(128, 128, 128);
             }
             QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {
                 background: none;
                 width: 0px;
                 height: 0px;
             }
             QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {
                 background: none;
             }
             QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
                 background: none;
             }
        """)

        self.layout = QGridLayout()
        self.layout.setContentsMargins(3, 3, 0, 3)
        self.layout.setSpacing(5)

        self.w = QWidget()
        self.w.setLayout(self.layout)
        self.scroll_area.setWidget(self.w)

        self.xmax = self.scroll_area.width() // (self.tile_size + 10)

        self.xmax -= 1
        dbcontrol.from_db_to_list()

        dbcontrol.meme_list.sort(key=self.sorting_date)

        self.create_labels()

    def tiles(self):
        """Компоновка изображений мемов в области"""
        ymax = 0
        while ymax * self.xmax < len(self.container):
            ymax += 1

        if self.xmax >= len(self.container):
            self.w.setFixedSize(len(self.container) * (self.tile_size + 10) - 15,
                                (self.tile_size + 10))
        else:
            self.w.setFixedSize(self.xmax * (self.tile_size + 10) - 15,
                                (self.tile_size + 10) * ymax)

        for i in reversed(range(self.layout.count())):
            self.layout.takeAt(i)

        for i, j in enumerate(self.container):
            self.layout.addWidget(j, i // self.xmax, i % self.xmax)

    def sorting_popularity(self, element):
        dbcontrol.cursor.execute("SELECT popularity FROM memes WHERE pic = ?", (element,))
        popularity = int(dbcontrol.cursor.fetchone()[0])
        return popularity

    def sorting_date(self, element):
        dbcontrol.cursor.execute("SELECT date FROM memes WHERE pic = ?", (element,))
        date = str(dbcontrol.cursor.fetchone()[0])
        return date

    def sorting_box_click(self, mode=0):
        if mode == 1:
            dbcontrol.meme_list.sort(key=self.sorting_date)
        elif mode == 2:
            dbcontrol.meme_list.sort(key=self.sorting_popularity)
            dbcontrol.meme_list.reverse()
        elif mode == 3:
            dbcontrol.meme_list.sort(key=self.sorting_popularity)
        else:
            dbcontrol.meme_list.sort(key=self.sorting_date)
            dbcontrol.meme_list.reverse()

        self.reinit()

    def moveWindow(self, event):
        """Перемещение окна"""
        if UIFunctions.returnStatus() == 1:
            UIFunctions.maximize_restore(self)

            screen_width = QtWidgets.QDesktopWidget().width()
            cursor_pos_x = QtGui.QCursor().pos().x()

            a = cursor_pos_x / screen_width

            self.move(int(cursor_pos_x - self.size().width() * a), 0)

        if event.buttons() == Qt.LeftButton:
            self.move(self.pos() + event.globalPos() - self.dragPos)
            self.dragPos = event.globalPos()
            event.accept()

    def tray_click(self, event):
        """Вызов окна при клике на иконку"""
        if event == QSystemTrayIcon.Trigger:
            self.show()

    def closeEvent(self, event):
        """Действие при закрытии"""
        if ui_settings.settings_data["hidetotray"]:
            event.ignore()
            self.hide()
            self.show_message("Приложение свёрнуто")
        else:
            del self.tray_icon

    def mousePressEvent(self, event):
        """Перемещение окна"""
        self.dragPos = event.globalPos()

    def resizeEvent(self, event):
        """Изменение размера окна"""
        self.resized.emit()
        self.sidepanel.hookResizeEvent()
        self.settings.hookResizeEvent()
        self.meme_frame.hookResizeEvent()

    def show_message(self, text):
        """Вывод уведомлений"""
        if ui_settings.settings_data["notifications"]:
            self.tray_icon.showMessage("Memes", text, QIcon(icon), 1000)

    def keyPressEvent(self, event: QKeyEvent):
        """Обработка нажатия клавиш ctr+V"""
        super().keyPressEvent(event)

        try:
            if int(event.modifiers()) == QtCore.Qt.ControlModifier:
                if event.key() == QtCore.Qt.Key_V and QApplication.clipboard().pixmap():
                    pixmap = QApplication.clipboard().pixmap()
                    file_text = QApplication.clipboard().text()[8:]
                    path = self.directory
                    copy_files = os.listdir(path)
                    files = os.listdir(path)
                    name = 1
                    while f"{name}.png" in files:
                        name += 1

                    if not pixmap.isNull():
                        pixmap.save(path + "/" + f"{name}.png", "PNG")
                        dbcontrol.from_folder_to_db(f"{name}.png")
                        dbcontrol.conn.commit()
                        self.show_message("Мем добавлен")
                        self.sorting_box_click(mode=0)
                        self.meme_frame.meme_press(f"{name}.png")
                        if ui_settings.settings_data["newtag"]:
                            self.meme_frame.editor_frame.tag_adder.tag_add.add_tag("неразмеченный")

                    elif (os.path.exists(file_text) and os.path.abspath(
                          os.path.dirname(file_text)) != path and os.path.basename(
                          file_text) not in copy_files and
                          not os.path.isdir(file_text)):

                        new_file = shutil.copy(file_text, self.directory).replace(
                                   self.directory, "")
                        dbcontrol.from_folder_to_db(new_file)
                        dbcontrol.conn.commit()
                        self.show_message("Мем добавлен")
                        self.sorting_box_click(mode=0)
                        self.meme_frame.meme_press(new_file)
                        if ui_settings.settings_data["newtag"]:
                            self.meme_frame.editor_frame.tag_adder.tag_add.add_tag("неразмеченный")
                    else:
                        self.show_message("Нет скопированного мема")

                    dbcontrol.update_desc()
                elif event.key() == QtCore.Qt.Key_C:
                    if self.meme_frame.isHidden():
                        if self.to_copy != None and not self.to_copy.isNull():
                            QtWidgets.QApplication.clipboard().setPixmap(self.to_copy)
                            self.show_message("Мем скопирован")
                        else:
                            self.show_message("Нечего копировать :(")
                    elif hasattr(self.meme_frame, "meme_image") and not self.meme_frame.meme_image.isNull():
                        QtWidgets.QApplication.clipboard().setPixmap(self.meme_frame.meme_image)
                        self.show_message("Мем скопирован")
                    else:
                        self.show_message("Ошибка при копировании мема :(")
            elif event.key() == QtCore.Qt.Key_F5:
                dbcontrol.meme_list.clear()
                dbcontrol.from_db_to_list()
                self.create_labels()
                self.reinit()
                dbcontrol.update_desc()
        except BaseException as error:
            logs.get_log(error)


if __name__ == "__main__":
    start_app()
