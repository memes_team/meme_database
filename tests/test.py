import sqlite3, os, sys, inspect


currentdir = os.path.dirname(os.path.dirname(os.path.abspath(
                             inspect.getfile(inspect.currentframe()))))


def test_db():
    conn = sqlite3.connect(currentdir + "/Database/MDatabase.db")
    cursor = conn.cursor()
    cursor.execute("INSERT INTO memes (pic) VALUES ('test.png');")
    a = list(cursor.execute("""SELECT pic FROM memes"""))[-1][-1]
    
    assert a == 'test.png'
