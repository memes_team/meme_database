import os.path
import shutil
import logs
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore
from Database import dbcontrol
from Interface import ui_settings


class DropArea(QScrollArea):
    """Класс окна виджета"""
    def __init__(self, parent=None):
        """Инициализация окна."""
        super().__init__(parent)
        self.parent = parent
        self.setAcceptDrops(True)

    def dropEvent(self, e):
        """Перетаскивание файла в окно виджета."""
        try:
            data = e.mimeData()
            files = data.text().split("\n")
            count = 0
            new_file = None

            path = self.parent.directory

            for i in files:
                if os.path.exists(i[8:]) and not os.path.isdir(i[8:]):
                    copy_files = os.listdir(path)

                    if (os.path.abspath(os.path.dirname(i[8:])) == path or
                            os.path.basename(i[8:]) in copy_files):
                        continue

                    count += 1
                    new_file = shutil.copy(i[8:], path).replace(path, "").replace("/", "").replace("\\", "")
                    dbcontrol.from_folder_to_db(new_file)
                    if ui_settings.settings_data["newtag"]:
                        self.parent.meme_frame.meme_press(new_file)
                        self.parent.meme_frame.editor_frame.tag_adder.tag_add.add_tag("неразмеченный")
                        self.parent.meme_frame.back_button_click()

            dbcontrol.conn.commit()
            self.parent.sorting_box_click(mode=0)

            if count == 0:
                self.parent.show_message("Мемы не добавлены")
            elif count == 1:
                self.parent.meme_frame.meme_press(new_file)
                self.parent.show_message("Мем добавлен")
            elif count > 1:
                self.parent.show_message("Мемы добавлены")

            dbcontrol.update_desc()
        except BaseException as error:
            logs.get_log(error)

    def dragEnterEvent(self, e):
        """Переопределение события на появление курсора в зоне виджета при перетаскивании."""
        e.accept()


class CopyButton(QPushButton):
    """Кнопка с функцией копирования изображения"""
    def __init__(self, parent, pixmap):
        """Инициализация кнопки"""
        super().__init__(parent)

        self.parent = parent
        self.pixmap = pixmap

        self.setMouseTracking(True)
        self.setCursor(QCursor(QtCore.Qt.PointingHandCursor))

        self.setToolTip('Копировать в буфер обмена')
        self.move(self.parent.parent.tile_size - 22, self.parent.parent.tile_size - 20)
        self.resize(20, 20)
        self.setIcon(QIcon("./Interface/Icons/copy_to_clipboard.svg"))
        self.setIconSize(QtCore.QSize(17, 17))
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        self.setStyleSheet("background-color: rgb(249, 249, 249)")

    def mousePressEvent(self, event):
        """Копирование при нажатии на кнопку"""
        if event.buttons() == QtCore.Qt.LeftButton:
            QApplication.clipboard().setPixmap(self.pixmap)

            dbcontrol.cursor.execute("SELECT popularity FROM memes WHERE pic = ?", (self.parent.image,))
            popularity = int(dbcontrol.cursor.fetchone()[0]) + 1
            dbcontrol.cursor.execute("UPDATE memes SET popularity=? WHERE pic=?", (popularity, self.parent.image))
            dbcontrol.conn.commit()

            self.parent.parent.show_message("Мем скопирован")


class Meme_Overview_Frame(QFrame):
    """Фрейм с картинкой мема"""
    def __init__(self, parent, image_path, image):
        """Инициализация фрейма"""
        super().__init__(parent)

        self.parent = parent
        self.image_path = image_path
        self.image = image
        self.setMouseTracking(True)

        self.label = QLabel(self)
        self.label.setCursor(QCursor(QtCore.Qt.PointingHandCursor))

        self.label.resize(self.parent.tile_size, self.parent.tile_size)

        self.pixmap = QPixmap(self.image_path + "/" + self.image)
        self.pixmap_scaled = self.pixmap.scaled(self.parent.tile_size, self.parent.tile_size,
                                                QtCore.Qt.KeepAspectRatio)

        self.label.move((self.parent.tile_size - self.pixmap_scaled.width()) // 2 + 1, 3)

        self.label.setPixmap(self.pixmap_scaled)

        self.copy_button = CopyButton(self, self.pixmap)

        self.bgcolor = (200, 200, 200)
        self.hovered = False
        self.bgcoloranimation = QtCore.QVariantAnimation(self)

        self.menu = QMenu(self)
        self.menu.addAction('Удалить мем')
        self.menu.triggered.connect(lambda: (self.parent.meme_frame.meme_press(self.image),
                                             self.parent.meme_frame.remove_button_click(mode=1)))

    def enterEvent(self, event):
        """Обработка наведения мыши"""
        self.parent.to_copy = self.pixmap

        self.hovered = True
        self.update()

    def animValueChanged(self, value):
        self.bgcolor = (value, value, value)
        self.update()

    def leaveEvent(self, event):
        """Анимация при отведении мыши"""
        self.parent.to_copy = None

        self.hovered = False
        if (hasattr(self, "bgcoloranimation") and self.bgcoloranimation is
                not None and self.bgcoloranimation.state() !=
                QtCore.QAbstractAnimation.Stopped):
            return

        self.bgcoloranimation = QtCore.QVariantAnimation(self)
        self.bgcoloranimation.valueChanged.connect(self.animValueChanged)
        self.bgcoloranimation.setDuration(500)
        self.bgcoloranimation.setStartValue(160)
        self.bgcoloranimation.setEndValue(200)
        self.bgcoloranimation.start()

    def mousePressEvent(self, event):
        """Открытие окна при нажатии мыши"""
        if event.buttons() == QtCore.Qt.LeftButton:
            self.parent.meme_frame.meme_press(self.image)
        if event.button() == QtCore.Qt.RightButton:
            self.menu.exec_(event.globalPos())

    def paintEvent(self, event):
        if self.hovered:
            self.bgcolor = (160, 160, 160)

        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        pen = QPen(QColor(*self.bgcolor), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QBrush(QColor(*self.bgcolor), QtCore.Qt.SolidPattern))
        qp.drawRoundedRect(0, 0, self.size().width(), self.size().height(), 4, 4)
        qp.end()
