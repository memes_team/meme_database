import logs
import os
from PyQt5 import QtCore, QtGui, QtWidgets
from .ui_sidepanel import SidePanelScrollArea, tags, TagState
from Database import dbcontrol


#controllers_frame_width = 160
controllers_frame_button_size = 45
editor_frame_width = 256  # 0 - right indent


def clamp(value, a, b):
    if value > b:
        return b
    elif value < a:
        return a

    return value


class Tag(QtWidgets.QFrame):
    def __init__(self, parent, name, meme_id=0, tag_id=0):
        super().__init__(parent)

        self.parent = parent
        self.name = name
        self.meme_id = meme_id
        self.tag_id = tag_id

        self.remove_button = QtWidgets.QPushButton(self)
        self.remove_button.setIcon(QtGui.QIcon("./Interface/Icons/delete_meme.png"))
        self.remove_button.clicked.connect(self.removeSelf)
        self.remove_button.setToolTip("Удалить тег")
        self.remove_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.remove_button.setStyleSheet("background-color: rgb(211, 64, 64)")

    def removeSelf(self):
        self.setParent(None)
        self.deleteLater()

        self.parent.contentsSize = 0

        for child in self.parent.children():
            child.move(0, self.parent.contentsSize)
            self.parent.contentsSize += 40

        dbcontrol.cursor.execute("DELETE FROM connect WHERE meme = ? AND tag = ?", (self.meme_id, self.tag_id))
        dbcontrol.conn.commit()

        dbcontrol.cursor.execute("SELECT * FROM connect where tag = ?", (self.tag_id,))
        count = len(dbcontrol.cursor.fetchall())

        if count <= 0:
            dbcontrol.cursor.execute("SELECT name FROM tags WHERE id = ?", (self.tag_id,))
            name = dbcontrol.cursor.fetchone()[0]

            for n, i in enumerate(tags):
                if i["name"] == name:
                    del tags[n]
                    if name in TagState:
                        del TagState[name]

            dbcontrol.cursor.execute("DELETE FROM tags WHERE id = ?", (self.tag_id,))
            dbcontrol.conn.commit()

        dbcontrol.update_tags()

        self.parent.parent().parent().parent().performTagsHolderLayout()
        self.parent.parent().parent().scrollContentsBy(0, 2)

    def resizeEvent(self, event):
        self.remove_button.setGeometry(self.width() - self.height(), 0, self.height(), self.height())

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)

        pen = QtGui.QPen(QtGui.QColor(64, 64, 64, 0), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(220, 220, 220))
        qp.drawRect(0, 0, self.width(), self.height())

        qp.setBrush(QtGui.QColor(195, 195, 195))
        qp.drawRect(0, self.height() - 2, self.width(), 2)

        qp.setPen(QtGui.QColor(39, 39, 39))
        qp.setFont(QtGui.QFont("Helvetica", 16))

        name = self.name

        if len(name) >= 16:
            name = name[0:16] + "..."

        qp.drawText(5, 0, self.width() - 5, self.height(), QtCore.Qt.AlignVCenter, name)
        qp.end()


class Add_Tag_Button(QtWidgets.QPushButton):
    def __init__(self, parent, data=""):
        super().__init__(parent)

        self.parent = parent
        self.tags_holder = self.parent.parent.tags_holder
        self.data = data

        self.setMouseTracking(True)
        self.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))

        self.bgcolor = (60, 254, 5)
        self.setToolTip("Добавить тег")
        self.setIcon(QtGui.QIcon("./Interface/Icons/save_changes.svg"))
        self.setStyleSheet("background-color: rgb(60, 254, 5)")
        self.selected = False
        self.hovered = False

    def enterEvent(self, event):
        self.hovered = True

    def leaveEvent(self, event):
        self.hovered = False

    def mousePressEvent(self, event):
        super().mousePressEvent(event)

        if event.buttons() == QtCore.Qt.LeftButton:
            self.add_tag()

    def add_tag(self, text=None):
        try:
            tag_name = text
            if text is None:
                tag_text = self.parent.tag_text
                tag_name = tag_text.text().lower()
            scrollarea = self.parent.parent.scrollarea

            if tag_name == "" or tag_name.replace(" ", "") == "":
                return

            for child in self.tags_holder.children():
                if child.name.lower() == tag_name:
                    self.parent.tag_text.setText("")

                    return

            id = 0

            for row in dbcontrol.cursor.execute("SELECT MAX(id) FROM tags"):
                id = row[0]

            should_write = True

            for row in dbcontrol.cursor.execute("SELECT * FROM tags WHERE name = ?", (tag_name,)):
                should_write = False

            if should_write:
                try:
                    id = int(id)
                except TypeError:
                    id = 0

                tag_id = id + 1

                tags.append({"name": tag_name, "popular": False})

                dbcontrol.cursor.execute("INSERT OR IGNORE INTO tags VALUES (?, ?, '')", (tag_id, tag_name))
                dbcontrol.conn.commit()
            else:
                dbcontrol.cursor.execute("SELECT id FROM tags WHERE name = ?", (tag_name,))

                tag_id = int(dbcontrol.cursor.fetchone()[0])

            #"""dbcontrol.cursor.execute("SELECT MAX(id) FROM connect")

            #res = dbcontrol.cursor.fetchone()
            #if res is None:
            #    id1 = 0
            #else:
            #    id1 = int(res[0])
            #
            #new_connection_id = id1 + 1"""

            dbcontrol.cursor.execute("SELECT id FROM memes WHERE pic = ?", (self.data,))

            meme_id = int(dbcontrol.cursor.fetchone()[0])

            dbcontrol.cursor.execute("INSERT OR IGNORE INTO connect VALUES (?, ?)", (meme_id, tag_id))
            dbcontrol.conn.commit()

            dbcontrol.update_tags()

            tag = Tag(self.tags_holder, tag_name, meme_id, tag_id)
            tag.setGeometry(0, self.tags_holder.contentsSize, self.tags_holder.width(), 40)
            self.tags_holder.contentsSize += 40
            tag.show()
            self.parent.parent.performTagsHolderLayout()

            scrollarea.scrollContentsBy(0, 0)

            self.parent.tag_text.setText("")
        except BaseException as error:
            logs.get_log(error)

    """def paintEvent(self, event):
        if self.hovered:
            self.bgcolor = (35, 229, 5)
        else:
            self.bgcolor = (60, 254, 5)

        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)

        pen = QtGui.QPen(QtGui.QColor(200, 200, 200), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(*self.bgcolor))
        qp.drawRect(0, 0, self.width(), self.height())

        # vert
        pen = QtGui.QPen(QtGui.QColor(39, 39, 39), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(39, 39, 39))
        qp.drawRoundedRect((self.height() - 4) // 2, 4, 4, self.height() - 8, 2, 2)

        # horiz
        pen = QtGui.QPen(QtGui.QColor(39, 39, 39), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(39, 39, 39))
        qp.drawRoundedRect(4, (self.width() - 4) // 2, self.width() - 8, 4, 2, 2)

        qp.end()"""


class QQLineEdit(QtWidgets.QLineEdit):
    def __init__(self, parent):
        super().__init__(parent)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Return:
            self.parent().tag_add.add_tag()
        else:
            super().keyPressEvent(event)


class Tag_Adder_Frame(QtWidgets.QFrame):
    def __init__(self, parent, data=""):
        super().__init__(parent)

        self.parent = parent

        self.tag_add = Add_Tag_Button(self, data)

        dbcontrol.cursor.execute("SELECT id FROM memes WHERE pic = ?", (data,))
        meme_id = int(dbcontrol.cursor.fetchone()[0])

        dbcontrol.cursor.execute("SELECT tag FROM connect WHERE meme = ?", (meme_id,))
        connected_tags = dbcontrol.cursor.fetchall()

        for tag_id in connected_tags:
            for row_name in dbcontrol.cursor.execute("SELECT name FROM tags WHERE id = ?", (tag_id[0],)):
                tag = Tag(self.tag_add.tags_holder, row_name[0], meme_id, tag_id[0])
                tag.setGeometry(0, self.tag_add.tags_holder.contentsSize, editor_frame_width, 40)
                self.tag_add.tags_holder.contentsSize += 40
                tag.show()
                self.tag_add.parent.parent.performTagsHolderLayout()

        completer_list = []

        for i in tags:
            completer_list.append(i["name"])

        self.completer = QtWidgets.QCompleter(completer_list, self)
        self.tag_text = QQLineEdit(self)
        self.tag_text.setCompleter(self.completer)
        self.tag_text.setFont(QtGui.QFont("Helvetica", 12))
        self.tag_text.setPlaceholderText("Мемный тег...")

    def resizeEvent(self, event):
        self.tag_add.setGeometry(self.width() - self.height(), 0, self.height(), self.height())
        self.tag_text.setGeometry(5, 5, self.width() - self.tag_add.width() - 10, self.height() - 10)


class Save_Desc_Button(QtWidgets.QPushButton):
    def __init__(self, parent):
        super().__init__(parent)

        self.meme_window = parent.parent

    def mousePressEvent(self, e):
        if self.meme_window.has_unsaved_desc:
            super().mousePressEvent(e)

    def paintEvent(self, event):
        super().paintEvent(event)

        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setFont(QtGui.QFont("Helvetica", 12))
        qp.drawText(0, 0, self.width(), self.height(), QtCore.Qt.AlignCenter, "Сохранить описание")
        qp.end()


class Editor_Frame(QtWidgets.QFrame):
    def __init__(self, parent, data=""):
        super().__init__(parent)

        self.data = data
        self.parent = parent

        self.description = QtWidgets.QTextEdit(self)
        self.description.setGeometry(5, 40 + 5, self.width() - 10, 128 - 45)
        self.description.setFont(QtGui.QFont("Helvetica", 12))

        dbcontrol.cursor.execute("SELECT description FROM memes WHERE pic = ?", (data,))
        description = dbcontrol.cursor.fetchall()[0][0]

        if description:
            self.description.setText(description)

        self.description.setPlaceholderText("Мемное описание...")
        self.description.textChanged.connect(self.textChangedLogic)

        self.button = Save_Desc_Button(self)
        self.button.setGeometry(5, 40 + 5 + self.description.height() + 5 + 5, self.width() - 10, 30)
        self.button.setStyleSheet("background-color: rgb(128, 128, 128)")
        self.button.setToolTip("Эта кнопка... сохраняет описание?")
        self.button.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.button.clicked.connect(self.update_db)

        self.tags_holder = QtWidgets.QFrame(self)
        self.tags_holder.setGeometry(0, 40 + 5 + self.description.height() + 40 + 5 + 5, self.width(),
                                     self.height() - (40 + 5 + self.description.height() + 40 + 5) - 2)
        self.tags_holder.setStyleSheet("background-color: rgb(230, 230, 230)")
        self.tags_holder.contentsSize = 0

        self.tag_adder = Tag_Adder_Frame(self, self.data)
        self.tag_adder.setGeometry(0, 40 + self.description.height() + 40 + 5 + 5 + 5, self.width(), 40)

        self.scrollarea = SidePanelScrollArea(self)
        self.scrollarea.setWidget(self.tags_holder)
        self.scrollarea.setFrameShadow(QtWidgets.QFrame.Raised)
        self.scrollarea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollarea.setStyleSheet("background-color: rgb(231, 231, 231)")

    def textChangedLogic(self):
        if dbcontrol.meme_desc[self.data] != self.description.toPlainText():
            self.button.setStyleSheet("background-color: rgb(60, 254, 5)")
            self.parent.has_unsaved_desc = True

            self.button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        else:
            self.button.setStyleSheet("background-color: rgb(128, 128, 128)")
            self.parent.has_unsaved_desc = False

            self.button.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.button.update()

    def update_db(self):
        if not self.parent.has_unsaved_desc:
            return

        try:
            self.button.setStyleSheet("background-color: rgb(128, 128, 128)")
            self.parent.has_unsaved_desc = False
            self.button.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))

            text = self.description.toPlainText()
            #text = text.replace("\'", "\'\'")
            dbcontrol.cursor.execute(
                "UPDATE memes SET description=? WHERE pic=?", (text, self.data))
            dbcontrol.conn.commit()

            dbcontrol.update_desc()
        except BaseException as error:
            logs.get_log(error)

    def resizeEvent(self, event):
        self.description.setGeometry(5, 40 + 5, self.width() - 10, 128 - 45)
        self.button.setGeometry(5, 40 + 5 + self.description.height() + 5 + 5, self.width() - 10, 30)
        #self.tag_adder.setGeometry(5, 40 + 5 + self.description.height() + 5 + 5, self.width() - 5 - 5, 30)
        self.tag_adder.setGeometry(0, 40 + self.description.height() + 40 + 5 + 5 + 5, self.width(), 40)
        self.performScrollAreaLayout()
        self.performTagsHolderLayout()

    def performTagsHolderLayout(self):
        height = self.height() - (40 + 5 + self.description.height() + 40 + 40 + 5 + 5) - 2

        if self.tags_holder.contentsSize > height:
            height = self.tags_holder.contentsSize

            for child in self.tags_holder.children():
                child.remove_button.move(child.width() - child.height() - 18, 0)
        else:
            for child in self.tags_holder.children():
                child.remove_button.move(child.width() - child.height(), 0)

        self.tags_holder.setGeometry(0, 40 + 5 + self.description.height() + 40 + 40 + 5 + 5, self.width(), height)

    def performScrollAreaLayout(self):
        self.scrollarea.setGeometry(0, 40 + 5 + self.description.height() + 40 + 40 + 5 + 5,
                                    self.width(),
                                    self.height() - (40 + 5 + self.description.height() + 40 + 40 + 5 + 5))

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        pen = QtGui.QPen(QtGui.QColor(230, 230, 230), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(230, 230, 230))
        qp.drawRect(0, 0, self.size().width(), self.size().height())

        pen = QtGui.QPen(QtGui.QColor(200, 200, 200), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(200, 200, 200))
        qp.drawRect(0, 0, self.size().width(), 40)

        qp.setPen(QtGui.QColor(39, 39, 39))
        qp.setFont(QtGui.QFont("Helvetica", 16))
        qp.drawText(5, 0, self.width() - 5, 40, QtCore.Qt.AlignVCenter, "Описание")

        pen = QtGui.QPen(QtGui.QColor(200, 200, 200), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(200, 200, 200))
        qp.drawRect(0, 40 + 5 + self.description.height() + 5, self.size().width(), 40)

        qp.drawRect(0, 40 + self.description.height() + 40 + 5 + 5 + 5, self.size().width(), 40)
        qp.end()


class Meme_Frame(QtWidgets.QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        self.setGeometry(0, 40, self.parent.size().width(), self.parent.size().height() - 40)
        self.hide()

    def meme_press(self, meme_image=""):
        if self.isHidden():
            self.show()
        else:
            for child in self.children():
                child.setParent(None)
                child.deleteLater()

            self.hide()

        self.has_unsaved_desc = False

        split = meme_image.split("/")

        self.img_name = split[len(split) - 1]

        self.controllers_frame = QtWidgets.QFrame(self)
        self.controllers_frame.setStyleSheet("background-color: rgb(249, 249, 249);")
        self.controllers_frame.show()

        self.meme_holder = QtWidgets.QFrame(self)
        self.meme_holder.setStyleSheet("background-color: rgb(190, 190, 190);")
        self.meme_holder.show()

        self.meme_displayer = QtWidgets.QLabel(self.meme_holder)

        self.meme_image = QtGui.QPixmap(self.parent.directory + "/" + meme_image)
        self.meme_image_scaled = self.meme_image.scaled(self.meme_holder.width(), self.meme_holder.height(),
                                                    QtCore.Qt.KeepAspectRatio)

        self.meme_displayer.setPixmap(self.meme_image_scaled)
        self.meme_displayer.show()

        self.editor_frame = Editor_Frame(self, self.img_name)
        #self.editor_frame.setStyleSheet("background-color: rgb(249, 0, 249);")
        self.editor_frame.show()

        self.controllers_frame.back_button = QtWidgets.QPushButton(self.controllers_frame)
        self.controllers_frame.back_button.setGeometry(self.controllers_frame.width() - 45 - 5, 5, 45, 45)
        self.controllers_frame.back_button.setText("")
        self.controllers_frame.back_button.setIcon(QtGui.QIcon("./Interface/Icons/back_arrow.svg"))
        self.controllers_frame.back_button.show()
        self.controllers_frame.back_button.setStyleSheet("background-color: rgb(211, 211, 211)")
        self.controllers_frame.back_button.setToolTip("Вернуться")
        self.controllers_frame.back_button.clicked.connect(self.back_button_click)

        self.controllers_frame.copy_button = QtWidgets.QPushButton(self.controllers_frame)
        self.controllers_frame.copy_button.setGeometry(self.controllers_frame.width() - 45 - 5, 5 + 45 + 5, 45, 45)
        self.controllers_frame.copy_button.setText("")
        self.controllers_frame.copy_button.setIcon(QtGui.QIcon("./Interface/Icons/copy_to_clipboard.svg"))
        self.controllers_frame.copy_button.show()
        self.controllers_frame.copy_button.setStyleSheet("background-color: rgb(211, 211, 211)")
        self.controllers_frame.copy_button.setToolTip("Копировать в буфер обмена")
        self.controllers_frame.copy_button.clicked.connect(self.copy_button_click)

        # self.controllers_frame.folder_button = QtWidgets.QPushButton(self.controllers_frame)
        # self.controllers_frame.folder_button.setGeometry(self.controllers_frame.width() - 45 - 5, 5 + 45 + 5 + 45 + 5, 45, 45)
        # self.controllers_frame.folder_button.setText("")
        # self.controllers_frame.folder_button.setIcon(QtGui.QIcon("./Interface/Icons/save_to_folder.svg"))
        # self.controllers_frame.folder_button.show()
        # self.controllers_frame.folder_button.setStyleSheet("background-color: rgb(211, 211, 211)")
        # self.controllers_frame.folder_button.setToolTip("Сохранить в папку")
        #self.controllers_frame.folder_button.clicked.connect(self.folder_button_click)

        self.controllers_frame.remove_button = QtWidgets.QPushButton(self.controllers_frame)
        self.controllers_frame.remove_button.setGeometry(self.controllers_frame.width() - 45 - 5,
                                                         5 + 45 + 5 + 45 + 5 + 45 + 5, 45, 45)
        self.controllers_frame.remove_button.setText("")
        self.controllers_frame.remove_button.setIcon(QtGui.QIcon("./Interface/Icons/delete_meme.png"))
        self.controllers_frame.remove_button.show()
        self.controllers_frame.remove_button.setStyleSheet("background-color: rgb(211, 64, 64)")
        self.controllers_frame.remove_button.setToolTip("Удалить мем навсегда и навечно... Перманентно.")
        self.controllers_frame.remove_button.clicked.connect(self.remove_button_click)

        self.controllers_frame.move(5, 5)
        self.framesPerformLayout()

    def copy_button_click(self):
        QtWidgets.QApplication.clipboard().setPixmap(self.meme_image)
        dbcontrol.cursor.execute("SELECT popularity FROM memes WHERE pic = ?", (self.img_name,))
        popularity = int(dbcontrol.cursor.fetchone()[0]) + 1
        dbcontrol.cursor.execute("UPDATE memes SET popularity=? WHERE pic=?", (popularity, self.img_name))
        dbcontrol.conn.commit()
        self.parent.show_message("Мем скопирован")

    def back_button_click(self):
        if self.has_unsaved_desc:
            dialog = QtWidgets.QDialog(self)

            dialog.setWindowFlag(QtCore.Qt.FramelessWindowHint)

            screen_width = QtWidgets.QDesktopWidget().width()
            screen_height = QtWidgets.QDesktopWidget().height()
            dialog_width = 355
            dialog_height = 100

            dialog.setGeometry((screen_width - dialog_width) // 2, (screen_height - dialog_height) // 2, dialog_width,
                               dialog_height)
            dialog.setMaximumSize(dialog_width, dialog_height)
            dialog.setMinimumSize(dialog_width, dialog_height)

            top_bar = QtWidgets.QFrame(dialog)
            top_bar.setGeometry(0, 0, dialog_width, 25)
            top_bar.setStyleSheet("background-color: rgb(108, 146, 220)")

            text = QtWidgets.QLabel(dialog)
            text.setGeometry(0, top_bar.height() + 5, dialog_width, dialog_height - (top_bar.height() + 5) - 35)

            text.setAlignment(QtCore.Qt.AlignCenter)
            text.setFont(QtGui.QFont("Helvetica", 11))
            text.setText("У вас остались несохранённые изменения.\nВсё равно выйти?")

            yes = QtWidgets.QPushButton(dialog)
            yes.setGeometry(5, 70, (dialog_width - 15) // 2, 25)
            yes.setText("Да")
            yes.clicked.connect(lambda: dialog.accept())

            no = QtWidgets.QPushButton(dialog)
            no.setGeometry(5 + (dialog_width - 15) // 2 + 5, 70, (dialog_width - 15) // 2, 25)
            no.setText("Нет")
            no.clicked.connect(lambda: dialog.reject())

            dialog.exec()

            if dialog.result() == 0:
                return

        for child in self.children():
            child.setParent(None)
            child.deleteLater()

        self.hide()
        self.parent.sidepanel.rebuildTags()
        self.parent.reinit()

    def remove_button_click(self, mode=0):
        if mode == 0:
            dialog = QtWidgets.QDialog(self)

            dialog.setWindowFlag(QtCore.Qt.FramelessWindowHint)

            screen_width = QtWidgets.QDesktopWidget().width()
            screen_height = QtWidgets.QDesktopWidget().height()
            dialog_width = 315
            dialog_height = 100

            dialog.setGeometry((screen_width - dialog_width) // 2, (screen_height - dialog_height) // 2, dialog_width, dialog_height)
            dialog.setMaximumSize(dialog_width, dialog_height)
            dialog.setMinimumSize(dialog_width, dialog_height)

            top_bar = QtWidgets.QFrame(dialog)
            top_bar.setGeometry(0, 0, dialog_width, 25)
            top_bar.setStyleSheet("background-color: rgb(108, 146, 220)")

            text = QtWidgets.QLabel(dialog)
            text.setGeometry(0, top_bar.height() + 5, dialog_width, dialog_height - (top_bar.height() + 5) - 35)

            text.setAlignment(QtCore.Qt.AlignCenter)
            text.setFont(QtGui.QFont("Helvetica", 11))
            text.setText("Вы действительно хотите удалить мем?")

            yes = QtWidgets.QPushButton(dialog)
            yes.setGeometry(5, 70, (dialog_width - 15) // 2, 25)
            yes.setText("Да")
            yes.clicked.connect(lambda: dialog.accept())

            no = QtWidgets.QPushButton(dialog)
            no.setGeometry(5 + (dialog_width - 15) // 2 + 5, 70, (dialog_width - 15) // 2, 25)
            no.setText("Нет")
            no.clicked.connect(lambda: dialog.reject())

            dialog.exec()

            if dialog.result() == 0:
                return
        if os.path.exists(self.parent.directory + "/" + self.img_name):
            os.remove(self.parent.directory + "/" + self.img_name)

        dbcontrol.cursor.execute("SELECT id, pic FROM memes WHERE pic = ?", (self.img_name,))
        fetch = dbcontrol.cursor.fetchone()
        meme_id = int(fetch[0])
        meme_name = fetch[1]

        dbcontrol.cursor.execute("SELECT tag FROM connect WHERE meme = ?", (meme_id,))

        for i in dbcontrol.cursor.fetchall():
            dbcontrol.cursor.execute("SELECT * FROM connect WHERE tag = ?", (*i,))
            count = len(dbcontrol.cursor.fetchall())

            if count <= 1:
                dbcontrol.cursor.execute("SELECT name FROM tags WHERE id = ?", (*i,))
                name = dbcontrol.cursor.fetchone()[0]

                for n, k in enumerate(tags):
                    if k["name"] == name:
                        del tags[n]
                        if name in TagState:
                            del TagState[name]

                dbcontrol.cursor.execute("DELETE FROM tags WHERE id = ?", (*i,))
                dbcontrol.conn.commit()

        dbcontrol.cursor.execute("DELETE FROM memes WHERE id = ?", (meme_id,))
        dbcontrol.cursor.execute("DELETE FROM connect WHERE meme = ?", (meme_id,))
        dbcontrol.conn.commit()

        dbcontrol.update_tags()

        try:
            dbcontrol.meme_list.remove(meme_name)
        except ValueError:
            pass

        self.back_button_click()

    def framesPerformLayout(self):
        self.controllers_frame.resize(clamp(self.size().width() - (self.size().height() + editor_frame_width) - 196, 55, 4096),
                                      self.size().height() - 5 - 5)

        controllers_frame_width = self.controllers_frame.size().width()
        self.controllers_frame.back_button.move(controllers_frame_width - 45 - 5, 5)
        self.controllers_frame.copy_button.move(controllers_frame_width - 45 - 5, 5 + 45 + 5)
        # self.controllers_frame.folder_button.move(controllers_frame_width - 45 - 5, 5 + 45 + 5 + 45 + 5)
        self.controllers_frame.remove_button.move(controllers_frame_width - 45 - 5, 5 + 45 + 5 + 45 + 5)

        self.meme_holder.setGeometry(controllers_frame_width + 5 + 5, 5,
                                        self.size().height() - 5 - 5,
                                        self.size().height() - 5 - 5)

        self.meme_displayer.resize(self.meme_holder.width(), self.meme_holder.height())
        self.meme_image_scaled = self.meme_image.scaled(self.meme_holder.width(), self.meme_holder.height(),
                                                    QtCore.Qt.KeepAspectRatio)

        self.meme_displayer.setPixmap(self.meme_image_scaled)
        self.meme_displayer.move((self.meme_holder.width() - self.meme_image_scaled.width()) // 2, 0)

        #self.editor_frame.setGeometry(5 + controllers_frame_width + 5 + self.meme_holder.size().width() + 5,
        self.editor_frame.setGeometry(self.width() - editor_frame_width - 5,
                                      5, editor_frame_width, self.size().height() - 5 - 5)
        self.editor_frame.scrollarea.scrollContentsBy(0, 0)

    def hookResizeEvent(self):
        self.setGeometry(0, 40, self.parent.size().width(), self.parent.size().height() - 40)

        if not self.isHidden():
            self.framesPerformLayout()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(249, 249, 249))
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        qp.end()
