from PyQt5 import QtCore, QtGui, QtWidgets
import sqlite3

tags = []

conn = sqlite3.connect("./Database/MDatabase.db")
cursor = conn.cursor()
for i in cursor.execute("""SELECT name FROM tags"""):
    tags.append({"name": i[0], "popular": False})

#tags[0]["popular"] = True
#tags[1]["popular"] = True

panel_status = False


class Category(QtWidgets.QPushButton):
    def __init__(self, parent, name):
        super().__init__(parent)

        self.name = name

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        # bg
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(200, 200, 200), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(200, 200, 200), QtCore.Qt.SolidPattern))
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        #
        #

        # cat name
        qp.setPen(QtGui.QColor(39, 39, 39))
        qp.setFont(QtGui.QFont("Helvetica", 16))
        qp.drawText(8, 0, self.width() - 8, self.height(), QtCore.Qt.AlignVCenter, self.name)
        #
        qp.end()


TagList = []

# !!!
TagState = {}


class Tag(QtWidgets.QPushButton):
    def __init__(self, parent, name):
        super().__init__(parent)

        self.parent = parent
        self.name = name
        # states: 0 - not selected, 1 - inclusive, 2 - exclusive
        self.state = 0
        self.hovered = False
        self.setMouseTracking(True)
        self.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))

        self.bgcolor = (231, 231, 231)

        TagList.append(self)

    def enterEvent(self, event):
        self.hovered = True

    def animValueChanged(self, value):
        self.bgcolor = (value, value, value)
        self.update()

    def leaveEvent(self, event):
        self.hovered = False
        if hasattr(self, "bgcoloranimation") and self.bgcoloranimation is not None and self.bgcoloranimation.state() != QtCore.QAbstractAnimation.Stopped:
            return

        self.bgcoloranimation = QtCore.QVariantAnimation(self)
        self.bgcoloranimation.valueChanged.connect(self.animValueChanged)
        self.bgcoloranimation.setDuration(500)
        self.bgcoloranimation.setStartValue(211)
        self.bgcoloranimation.setEndValue(231)
        self.bgcoloranimation.start()

    def mousePressEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.state += 1

            if self.state > 2:
                self.state = 0
        elif event.buttons() == QtCore.Qt.RightButton:
            self.state = 0

        for button in TagList:
            if button.name == self.name:
                button.state = self.state
                button.update()

        TagState[self.name] = self.state

        # прапрапрадедушка
        self.parent.parent().parent().parent().parent.reinit()

        self.update()

    def paintEvent(self, e):
        if self.hovered or self.state != 0:
            self.bgcolor = (211, 211, 211)

        qp = QtGui.QPainter()
        qp.begin(self)
        # bg
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(231, 231, 231), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(*self.bgcolor), QtCore.Qt.SolidPattern))
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        #

        if self.state == 0:
            box_color = (255, 255, 255)
        if self.state == 1:
            box_color = (0, 196, 0)
        elif self.state == 2:
            box_color = (196, 0, 0)
        # box
        pen = QtGui.QPen(QtGui.QColor(0, 0, 0), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(*box_color), QtCore.Qt.SolidPattern))
        qp.drawRect(5, 5, self.size().height() - 10, self.size().height() - 10)
        #

        # tag name
        qp.setPen(QtGui.QColor(39, 39, 39))
        qp.setFont(QtGui.QFont("Helvetica", 13))
        qp.drawText(5 + (self.size().height() - 10) + 5, 0, self.width(), self.height(), QtCore.Qt.AlignLeft, self.name)
        #
        qp.end()


class SidePanelScrollArea(QtWidgets.QScrollArea):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        stylesheet = """
            QScrollBar:vertical {
                 background: rgb(231, 231, 231);
             }
             QScrollBar::handle:vertical {
                 background: rgb(160, 160, 160);
                 border-radius: 4px;
                 margin-left: 3px;
                 margin-right: 2px;
                 min-height: 20px;
             }
             QScrollBar::handle:vertical:hover {
                 background: rgb(128, 128, 128);
             }
             QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {
                 background: none;
                 width: 0px;
                 height: 0px;
             }
             QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {
                 background: none;
             }
             QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
                 background: none;
             }
        """

        self.verticalScrollBar().setStyleSheet(stylesheet)


class SidePanel(QtWidgets.QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.visibleWidth = 273
        self.hidden = True
        self.parent = parent

        self.setGeometry(self.parent.size().width(), 40,
                         self.parent.size().width(), self.parent.size().height() - 40)

        self.performLayout()

        self.btn_tags = QtWidgets.QPushButton(self)
        self.btn_tags.setMinimumSize(QtCore.QSize(38, 38))
        self.btn_tags.setMaximumSize(QtCore.QSize(38, 38))
        self.btn_tags.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_tags.setStyleSheet("QPushButton {\n"
                                    "    qproperty-icon: url(\" \"); \n"
                                    "    qproperty-iconSize: 25px 25px;\n"
                                    "    background-image: url(./Interface/Icons/rightarrow.svg);\n"
                                    "    background-repeat: no-repeat;\n"
                                    "    border: none;\n"
                                    "    background-color: rgb(231, 231, 231);\n"
                                    "    background-position: right;\n"
                                    "}")
        self.btn_tags.setText("")
        self.btn_tags.setIconSize(QtCore.QSize(25, 25))
        self.btn_tags.setGeometry(0, 8, 38, 38)

        self.buttonFunctionality(self.btn_tags)

        self.search_line = QtWidgets.QLineEdit(self)
        self.search_line.setMinimumSize(QtCore.QSize(0, 38))
        self.search_line.setMaximumSize(QtCore.QSize(16777215, 38))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.search_line.setFont(font)
        self.search_line.setStyleSheet("QLineEdit {\n"
"    padding: 5px;\n"
"    color: rgb(39, 39, 39);\n"
"    border: 1px solid rgb(212, 212, 212);\n"
"    background-image: url(:/search_image/Icons/search.svg);\n"
"    background-position: top right;\n"
"    background-repeat: no-repeat;\n"
"    background-origin: content;\n"
"}\n"
"\n"
"QLineEdit:hover {\n"
"    border: 1px solid rgb(111, 111, 111);\n"
"}\n"
"\n"
"QLineEdit:focus {\n"
"    border: 1px solid rgb(111, 111, 111);\n"
"}")
        self.search_line.setGeometry(self.btn_tags.size().width() + 5, 8,
                                     self.visibleWidth - self.btn_tags.size().width() - 10, 38)
        self.search_line.setPlaceholderText("Фильтр по тегам")
        self.search_line.textChanged.connect(self.onSearchFieldChanged)

        self.tags_frame = QtWidgets.QFrame(self)
        self.tags_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.tags_frame.setFrameShadow(QtWidgets.QFrame.Raised)

        #self.tags_frame_layout = QtWidgets.QVBoxLayout(self.tags_frame)
        #self.tags_frame_layout.setContentsMargins(0, 0, 0, 0)
        #self.tags_frame_layout.setSpacing(0)

        #self.rebuildTags()

        self.scrollarea = SidePanelScrollArea(self)
        self.scrollarea.setWidget(self.tags_frame)
        #self.scrollarea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollarea.setFrameShadow(QtWidgets.QFrame.Raised)
        self.scrollarea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollarea.setStyleSheet("background-color: rgb(231, 231, 231)")
        self.rebuildTags()

    def onSearchFieldChanged(self, text):
        self.rebuildTags(text)

    def rebuildTags(self, search=""):
        for child in self.tags_frame.children():
            child.setParent(None)
            child.deleteLater()

        global TagList
        TagList = []

        self.tags_frame.contentsSize = 0

        if search == "":
            # self.tags_frame.popular = Category(self.tags_frame, "Популярные")
            # self.tags_frame.popular.setGeometry(0, 0, self.visibleWidth, 36)
            # self.tags_frame.popular.show()
            # self.tags_frame.contentsSize += 36

            # We should read tags here
            for i in tags:
                if i["popular"] and i["name"].lower().find(search.lower()) != -1:
                    btn = Tag(self.tags_frame, i["name"])
                    btn.setGeometry(0, self.tags_frame.contentsSize, self.visibleWidth, 24)

                    btn.show()

                    if btn.name in TagState:
                        btn.state = TagState[btn.name]

                    self.tags_frame.contentsSize += 24

        self.tags_frame.alphabetical = Category(self.tags_frame, "По алфавиту")
        self.tags_frame.alphabetical.setGeometry(0, self.tags_frame.contentsSize, self.visibleWidth, 36)
        self.tags_frame.alphabetical.show()
        self.tags_frame.contentsSize += 36

        # We should read tags here
        for i in tags:
            if i["name"].lower().find(search.lower()) != -1:
                btn = Tag(self.tags_frame, i["name"])
                btn.setGeometry(0, self.tags_frame.contentsSize, self.visibleWidth, 24)

                btn.show()

                if btn.name in TagState:
                    btn.state = TagState[btn.name]

                self.tags_frame.contentsSize += 24

        self.performTagsFrameLayout()
        self.scrollarea.scrollContentsBy(0, 0)

    def buttonFunctionality(self, button):
        button.clicked.connect(self.btnFunc)

    def btnFunc(self):
        self.hidden = not self.hidden

        self.parent.panel_status = not self.parent.panel_status
        self.parent.edit_size()

        self.performLayout()

    def hookResizeEvent(self):
        self.performLayout(noanimation=True)
        self.performTagsFrameLayout()

    def performLayout(self, noanimation=False):
        animspeed = 250
        if noanimation:
            animspeed = 0

        if self.hidden:
            self.animation = QtCore.QPropertyAnimation(self, b"geometry")
            self.animation.setDuration(animspeed)
            self.animation.setStartValue(QtCore.QRect(self.geometry()))
            self.animation.setEndValue(QtCore.QRect(self.parent.size().width(), 40,
                                                    self.parent.size().width(), self.parent.size().height() - 40))
            self.animation.setEasingCurve(QtCore.QEasingCurve.InCubic)
            self.animation.start()
            #self.setGeometry(self.parent.size().width(), 40,
            #                 self.parent.size().width(), self.parent.size().height() - 40)
        else:
            self.animation = QtCore.QPropertyAnimation(self, b"geometry")
            self.animation.setDuration(animspeed)
            self.animation.setStartValue(QtCore.QRect(self.geometry()))
            self.animation.setEndValue(QtCore.QRect(self.parent.size().width() - self.visibleWidth, 40,
                                                    self.parent.size().width(), self.parent.size().height() - 40))
            self.animation.setEasingCurve(QtCore.QEasingCurve.OutQuart)
            self.animation.start()

            #self.setGeometry(animation.currentValue())

    def performTagsFrameLayout(self):
        height = self.size().height() - (8 + self.search_line.size().height() + 5) - 2

        if self.tags_frame.contentsSize > self.size().height() - (8 + self.search_line.size().height() + 5) - 2:
            height = self.tags_frame.contentsSize

        self.tags_frame.setGeometry(0, 8 + self.search_line.size().height() + 5,
                                    self.visibleWidth, height)

        self.scrollarea.setGeometry(0, 8 + self.search_line.size().height() + 5,
                                    self.visibleWidth,
                                    self.size().height() - (8 + self.search_line.size().height() + 5))

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        # 212
        pen = QtGui.QPen(QtGui.QColor(231, 231, 231), QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(231, 231, 231))
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        qp.end()
