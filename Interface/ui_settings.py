import os
import shutil
from PyQt5 import QtCore, QtGui, QtWidgets
from Database import dbcontrol

settings_categories = {
    "main": "Основные",
    "ui": "Внешний вид"
}

settings = {
    "main": [
        [
            "Тег «неразмеченный»",
            "Автоматически добавлять тег новым загрузкам",
            "button",
            "newtag"
        ],
        [
            "Копировать при поиске",
            "Автоматически копировать в буфер наиболее подходящий мем при поиске",
            "button",
            "searchcopybest"
        ],
        # [
        #     "Текст мема в описание",
        #     "Автоматически добавлять текст с мема в его описание",
        #     "button",
        #     "memetexttodescription"
        # ],
        [
            "Директория хранения мемов",
            "",
            "folder",
            "memefolder"
        ]
    ],
    "ui": [
        # [
        #     "Тёмная тема",
        #     "Меняет внешний вид программы",
        #     "button",
        #     "darktheme"
        # ],
        [
            "Сворачивать в трей",
            "Сворачивать приложение в трей при закрытии окна",
            "button",
            "hidetotray"
        ],
        [
            "Показывать уведомления",
            "Показывать системные уведомления о событиях",
            "button",
            "notifications"
        ],
        [
            "Размер плиток мемов",
            "",
            "memeslider",
            "memesize"
        ]
    ]
}

# !!!
# import ui_settings != from ui_settings import settings_data
# !!!
settings_data = {
    "newtag": 0,
    "searchcopybest": 0,
    "memetexttodescription": 0,
    "memefolder": os.path.abspath("./Database/images/"),
    "darktheme": 0,
    "memesize": 128,
    "hidetotray": 0,
    "notifications": 1
}

settings_controller = QtCore.QSettings("settings.ini", QtCore.QSettings.IniFormat)

# sql query settings here
def query_settings():
    # Если новых настроек не существует в таблице, то создаём их.
    for setting, value in settings_data.items():
        if not settings_controller.contains(setting):
            settings_controller.setValue(setting, value)
        #dbcontrol.cursor.execute(f"INSERT OR IGNORE INTO settings VALUES ('{setting}', '{value}')")
        #dbcontrol.conn.commit()

    #for row in dbcontrol.cursor.execute("SELECT * FROM settings"):
    for setting in settings_controller.allKeys():
        value = settings_controller.value(setting, settings_data[setting])

        try:
            value = int(value)
        except ValueError:
            value = str(value)

        settings_data[setting] = value


def update_setting(setting, value):
    settings_data[setting] = value
    settings_controller.setValue(setting, value)

    settings_controller.sync()
    #dbcontrol.cursor.execute(f"UPDATE settings SET value = '{value}' WHERE setting = '{setting}'")
    #dbcontrol.conn.commit()


query_settings()

settings_divider = 256


class Settings_Folder_Selector(QtWidgets.QPushButton):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent
        self.w = 64
        self.h = 32

        self.setGeometry((self.parent.size().width() - self.w) / 2, (self.parent.size().height() - self.h) / 2,
                         self.w, self.h)

        self.setMouseTracking(True)
        self.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))

        self.show()

    def mousePressEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            dirlist = QtWidgets.QFileDialog.getExistingDirectory(self, "Выбрать папку", ".")

            if dirlist != "":
                update_setting("memefolder", dirlist)

            self.parent.parent().long_desc_frame.label_frame.setText(f"Текущая папка: {settings_data['memefolder']}")

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(211, 211, 211), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(211, 211, 211), QtCore.Qt.SolidPattern))
        qp.drawRoundedRect(0, 0, self.size().width(), self.size().height(), 16, 16)

        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawEllipse(12, 12, 8, 8)

        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawEllipse(28, 12, 8, 8)

        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawEllipse(44, 12, 8, 8)
        qp.end()


class Settings_Slider_Frame(QtWidgets.QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        self.setGeometry(0, 40, self.parent.size().width(), self.parent.size().height() - 40)

        self.slider = QtWidgets.QSlider(self)
        self.slider.setGeometry(0, 0, self.parent.short_desc_frame.label_frame.size().width(), 16)
        self.slider.setMinimum(64)
        self.slider.setMaximum(196)
        self.slider.setValue(settings_data["memesize"])
        self.slider.setOrientation(QtCore.Qt.Horizontal)

        stylesheet = """
            QSlider::handle:horziontal {
                border-radius: 4px;
                background: rgb(211, 211, 211);
                margin: 0 -4px; /* expand outside the groove */
            }
        """

        self.slider.setStyleSheet(stylesheet)
        self.slider.valueChanged.connect(self.sliderValueChanged)
        self.slider.sliderReleased.connect(self.sliderReleased)

    def sliderValueChanged(self, value):
        settings_data["memesize"] = value
        self.update()

    def sliderReleased(self):
        update_setting("memesize", self.slider.value())

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(190, 190, 190), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190), QtCore.Qt.SolidPattern))
        qp.drawRect((self.slider.maximum() - settings_data["memesize"]) / 2,
                    (self.size().height() - settings_data["memesize"]) / 2,
                    settings_data["memesize"], settings_data["memesize"])

        qp.setPen(QtGui.QColor(39, 39, 39))
        qp.setFont(QtGui.QFont("Helvetica", 18 * (settings_data["memesize"]/self.slider.maximum())))
        qp.drawText((self.slider.maximum() - settings_data["memesize"]) / 2,
                    (self.size().height() - settings_data["memesize"]) / 2,
                    settings_data["memesize"], settings_data["memesize"], QtCore.Qt.AlignCenter, "*Продам мем*")
        qp.end()


class Settings_Button(QtWidgets.QPushButton):
    def __init__(self, parent, settings_var):
        super().__init__(parent)

        self.parent = parent
        self.settings_var = settings_var
        self.w = 64
        self.h = 32

        self.setGeometry((self.parent.size().width() - self.w) / 2, (self.parent.size().height() - self.h) / 2,
                         self.w, self.h)

        self.setMouseTracking(True)
        self.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.state = settings_data[self.settings_var]

        if self.state == 1:
            self.circle_pos = self.w - 28 - 2

            self.alpha = 0
        else:
            self.circle_pos = 4

            self.alpha = 255

        self.show()

    def animValueChanged(self, value):
        self.circle_pos = value
        self.update()

    def alphaValueChanged(self, value):
        self.alpha = value
        #self.update()

    def mousePressEvent(self, event):
        if hasattr(self, "circle_posanimation") and self.circle_posanimation is not None and self.circle_posanimation.state() != QtCore.QAbstractAnimation.Stopped:
            return

        if event.buttons() == QtCore.Qt.LeftButton:
            if self.state == 1:
                self.state = 0

                self.circle_posanimation = QtCore.QVariantAnimation(self)
                self.circle_posanimation.valueChanged.connect(self.animValueChanged)
                self.circle_posanimation.setDuration(350)
                self.circle_posanimation.setStartValue(self.circle_pos)
                self.circle_posanimation.setEndValue(4)
                self.circle_posanimation.setEasingCurve(QtCore.QEasingCurve.InCubic)
                self.circle_posanimation.start()

                self.alpha_posanimation = QtCore.QVariantAnimation(self)
                self.alpha_posanimation.valueChanged.connect(self.alphaValueChanged)
                self.alpha_posanimation.setDuration(350)
                self.alpha_posanimation.setStartValue(0)
                self.alpha_posanimation.setEndValue(255)
                self.alpha_posanimation.setEasingCurve(QtCore.QEasingCurve.InCubic)
                self.alpha_posanimation.start()
            elif self.state == 0:
                self.state = 1

                self.circle_posanimation = QtCore.QVariantAnimation(self)
                self.circle_posanimation.valueChanged.connect(self.animValueChanged)
                self.circle_posanimation.setDuration(350)
                self.circle_posanimation.setStartValue(self.circle_pos)
                self.circle_posanimation.setEndValue(self.w - 28 - 2)
                self.circle_posanimation.setEasingCurve(QtCore.QEasingCurve.InCubic)
                self.circle_posanimation.start()

                self.alpha_posanimation = QtCore.QVariantAnimation(self)
                self.alpha_posanimation.valueChanged.connect(self.alphaValueChanged)
                self.alpha_posanimation.setDuration(350)
                self.alpha_posanimation.setStartValue(255)
                self.alpha_posanimation.setEndValue(0)
                self.alpha_posanimation.setEasingCurve(QtCore.QEasingCurve.InCubic)
                self.alpha_posanimation.start()

            update_setting(self.settings_var, self.state)

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)

        pen = QtGui.QPen(QtGui.QColor(56, 218, 48), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(56, 218, 48), QtCore.Qt.SolidPattern))
        qp.drawRoundedRect(0, 0, self.size().width(), self.size().height(), 16, 16)

        pen = QtGui.QPen(QtGui.QColor(211, 211, 211, self.alpha), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(211, 211, 211, self.alpha), QtCore.Qt.SolidPattern))
        qp.drawRoundedRect(0, 0, self.size().width(), self.size().height(), 16, 16)

        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawEllipse(self.circle_pos, 3, 26, 26)
        qp.end()


class Settings_Info(QtWidgets.QFrame):
    def __init__(self, parent, short_desc, long_desc, type, settings_var):
        super().__init__(parent)

        self.parent = parent

        if type == "memeslider":
            self.h = 260
        else:
            self.h = 80

        self.setGeometry(10, self.parent.posy, parent.size().width() - 10, self.h)

        self.parent.posy += self.h

        self.short_desc_frame = QtWidgets.QFrame(self)
        self.short_desc_frame.setGeometry(0, 0, self.size().width(), 40)

        short_font = QtGui.QFont()
        short_font.setFamily("Helvetica")
        #hort_font.setBold(True)
        short_font.setPointSize(17)

        self.short_desc_frame.label_frame = QtWidgets.QLabel(self.short_desc_frame)
        self.short_desc_frame.label_frame.setGeometry(0, 0, 256,
                                                     self.short_desc_frame.size().height())
        self.short_desc_frame.label_frame.setText(short_desc)
        self.short_desc_frame.label_frame.setFont(short_font)
        self.short_desc_frame.label_frame.adjustSize()

        if type == "memeslider":
            self.slider_frame = Settings_Slider_Frame(self)
        else:
            long_font = QtGui.QFont()
            long_font.setFamily("Helvetica")
            long_font.setPointSize(10)

            self.long_desc_frame = QtWidgets.QFrame(self)
            self.long_desc_frame.setGeometry(0, 40, self.size().width(), self.size().height() - 40)

            self.long_desc_frame.label_frame = QtWidgets.QLabel(self.long_desc_frame)
            self.long_desc_frame.label_frame.setGeometry(0, 0, self.long_desc_frame.size().width(),
                                                         self.long_desc_frame.size().height())
            self.long_desc_frame.label_frame.setText(long_desc)
            self.long_desc_frame.label_frame.setFont(long_font)
            self.long_desc_frame.label_frame.setAlignment(QtCore.Qt.AlignJustify)

            self.button_frame = QtWidgets.QFrame(self)
            self.button_frame.setGeometry(self.size().width() - 80, 0, 80, 80)

        self.show()

        if type == "button":
            Settings_Button(self.button_frame, settings_var)
        elif type == "folder":
            Settings_Folder_Selector(self.button_frame)
            self.long_desc_frame.label_frame.setText(f"Текущая папка: {settings_data['memefolder']}")
        elif type == "memeslider":
            pass

    def resizeEvent(self, event):
        if hasattr(self, "long_desc_frame") and self.long_desc_frame is not None:
            self.long_desc_frame.resize(self.size().width(), self.size().height() - 40)
            self.long_desc_frame.label_frame.resize(self.long_desc_frame.size().width(),
                                                      self.long_desc_frame.size().height())

        if hasattr(self, "button_frame") and self.button_frame is not None:
            self.button_frame.setGeometry(self.size().width() - 80, 0, 80, 80)

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        pen = QtGui.QPen(QtGui.QColor(232, 232, 232), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(232, 232, 232), QtCore.Qt.SolidPattern))
        qp.drawRoundedRect(0, self.size().height() - 3, self.size().width(), 3, 8, 8)
        qp.end()


class Settings_Holder(QtWidgets.QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        self.setGeometry(settings_divider, 0, self.parent.size().width() - settings_divider, self.parent.size().height())
        self.posy = 0

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawRect(0, 0, self.size().width(), self.size().height())

        pen = QtGui.QPen(QtGui.QColor(232, 232, 232), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(232, 232, 232), QtCore.Qt.SolidPattern))
        qp.drawRect(0, 0, 2, self.size().height())
        qp.end()

    def addSetting(self, short_desc, long_desc, type, settings_var):
        Settings_Info(self, short_desc, long_desc, type, settings_var)

    def clear(self):
        self.posy = 0

        for child in self.children():
            child.setParent(None)
            child.deleteLater()

    def hookResizeEvent(self):
        self.setGeometry(settings_divider, 0, self.parent.size().width() - settings_divider, self.parent.size().height())

        for child in self.children():
            child.resize(self.size().width() - 10, child.h)

class Category_Button(QtWidgets.QPushButton):
    def __init__(self, parent, id, display_name):
        super().__init__(parent)

        self.parent = parent
        self.id = id
        self.display_name = display_name

        self.setGeometry(0, 0, self.parent.size().width(), 36)

        self.setMouseTracking(True)
        self.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))

        self.bgcolor = (249, 249, 249)
        self.selected = False
        self.hovered = False

    def enterEvent(self, event):
        self.hovered = True

    def leaveEvent(self, event):
        self.hovered = False

    def mousePressEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.select()

    def select(self):
        for cat in self.parent.cats:
            cat.selected = False

        self.selected = True

        settings_holder = self.parent.parent().settings_holder_frame
        settings_holder.clear()

        for setting in settings[self.id]:
            settings_holder.addSetting(setting[0], setting[1], setting[2], setting[3])

        for cat in self.parent.cats:
            cat.update()

    def paintEvent(self, event):
        if self.hovered or self.selected:
            self.bgcolor = (211, 211, 211)
        else:
            self.bgcolor = (249, 249, 249)

        qp = QtGui.QPainter()
        qp.begin(self)
        # bg
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(*self.bgcolor), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(*self.bgcolor), QtCore.Qt.SolidPattern))
        #qp.drawRoundedRect(0, 0, self.size().width(), self.size().height(), 8, 8)
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        #qp.drawRoundedRect(4, 0, self.size().width() + 16 - 4, self.size().height(), 16, 16)
        #
        #

        # cat name
        qp.setPen(QtGui.QColor(39, 39, 39))
        qp.setFont(QtGui.QFont("Helvetica", 16))
        qp.drawText(8, 0, self.width() - 8, self.height(), QtCore.Qt.AlignVCenter, self.display_name)
        #
        qp.end()


class Settings_Categories(QtWidgets.QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.setGeometry(0, 0, settings_divider, parent.size().height())
        self.posy = 0
        self.cats = []

        for id, display_name in settings_categories.items():
            cat = Category_Button(self, id, display_name)
            cat.move(0, self.posy)

            self.posy += cat.size().height() + 2
            self.cats.append(cat)

        if len(self.cats) > 0:
            self.cats[0].select()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        pen = QtGui.QPen(QtGui.QColor(249, 249, 249), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        qp.end()


class Settings_Frame(QtWidgets.QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        self.setGeometry(0, 40, self.parent.size().width(), self.parent.size().height() - 40)

        self.settings_holder_frame = Settings_Holder(self)
        self.settings_categories_frame = Settings_Categories(self)
        #self.settings_holder_frame = Settings_Holder(self)

        self.hide()

    def buttonFunctionality(self, button):
        button.clicked.connect(self.btnFunc)

    def btnFunc(self):
        if self.isHidden():
            self.show()
        else:
            self.parent.tile_size = settings_data["memesize"]

            if (self.parent.directory and settings_data["memefolder"] and
                    self.parent.directory != settings_data["memefolder"]):

                for i in os.listdir(self.parent.directory):
                    if not os.path.exists(settings_data["memefolder"] + "/" + i):
                        shutil.move(self.parent.directory + "/" + i, settings_data["memefolder"])
                if len(os.listdir(self.parent.directory)) <= 0:
                    shutil.rmtree(self.parent.directory)


            self.parent.directory = settings_data["memefolder"]
            self.parent.reinit()

            self.hide()

    def hookResizeEvent(self):
        self.setGeometry(0, 40, self.parent.size().width(), self.parent.size().height() - 40)
        self.settings_holder_frame.hookResizeEvent()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        pen = QtGui.QPen(QtGui.QColor(231, 231, 231), QtCore.Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(QtGui.QBrush(QtGui.QColor(249, 249, 249), QtCore.Qt.SolidPattern))
        qp.drawRect(0, 0, self.size().width(), self.size().height())
        qp.end()
