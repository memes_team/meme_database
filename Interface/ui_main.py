# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_main.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from Interface import ui_settings
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.mainwindow = MainWindow

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1280, 720)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/window_icon_image/Icons/window_icon.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMinimumSize(QtCore.QSize(1000, 563))
        self.centralwidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(16)
        self.centralwidget.setFont(font)
        self.centralwidget.setObjectName("centralwidget")
        self.backgroung_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.backgroung_layout.setContentsMargins(0, 0, 0, 0)
        self.backgroung_layout.setSpacing(0)
        self.backgroung_layout.setObjectName("backgroung_layout")
        self.background_frame = QtWidgets.QFrame(self.centralwidget)
        self.background_frame.setStyleSheet("background-color: rgb(249, 249, 249);")
        self.background_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.background_frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.background_frame.setLineWidth(1)
        self.background_frame.setObjectName("background_frame")
        self.background_layout = QtWidgets.QVBoxLayout(self.background_frame)
        self.background_layout.setContentsMargins(0, 0, 0, 0)
        self.background_layout.setSpacing(0)
        self.background_layout.setObjectName("background_layout")
        self.title_bar = QtWidgets.QFrame(self.background_frame)
        self.title_bar.setMaximumSize(QtCore.QSize(16777215, 40))
        self.title_bar.setStyleSheet("background-color: rgb(108, 146, 220);")
        self.title_bar.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.title_bar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.title_bar.setObjectName("title_bar")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.title_bar)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame_btns_left = QtWidgets.QFrame(self.title_bar)
        self.frame_btns_left.setMinimumSize(QtCore.QSize(40, 40))
        self.frame_btns_left.setMaximumSize(QtCore.QSize(40, 40))
        self.frame_btns_left.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_btns_left.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_btns_left.setObjectName("frame_btns_left")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_btns_left)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.btn_settings = QtWidgets.QPushButton(self.frame_btns_left)
        self.btn_settings.setMinimumSize(QtCore.QSize(40, 40))
        self.btn_settings.setMaximumSize(QtCore.QSize(40, 40))
        self.btn_settings.setStyleSheet(
            "QPushButton {\n"
            "    border: none;\n"
            "}\n"
            "QPushButton:hover {\n"
            "    background-color: rgb(99, 135, 202);\n"
            "}")
        self.btn_settings.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/settings_image/Icons/settings.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_settings.setIcon(icon1)
        self.btn_settings.setIconSize(QtCore.QSize(30, 30))
        self.btn_settings.setObjectName("btn_settings")
        self.horizontalLayout_2.addWidget(self.btn_settings)
        # self.btn_help = QtWidgets.QPushButton(self.frame_btns_left)
        # self.btn_help.setMinimumSize(QtCore.QSize(40, 40))
        # self.btn_help.setMaximumSize(QtCore.QSize(40, 40))
        # self.btn_help.setStyleSheet(
        #     "QPushButton {\n"
        #     "    border: none;\n"
        #     "}\n"
        #     "QPushButton:hover {\n"
        #     "    background-color: rgb(99, 135, 202);\n"
        #     "}")
        # self.btn_help.setText("")
        # icon2 = QtGui.QIcon()
        # icon2.addPixmap(QtGui.QPixmap(":/help_image/Icons/help.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.btn_help.setIcon(icon2)
        # self.btn_help.setIconSize(QtCore.QSize(30, 30))
        # self.btn_help.setObjectName("btn_help")
        # self.horizontalLayout_2.addWidget(self.btn_help)
        self.horizontalLayout.addWidget(self.frame_btns_left)
        self.frame_title = QtWidgets.QFrame(self.title_bar)
        self.frame_title.setMinimumSize(QtCore.QSize(0, 40))
        self.frame_title.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.frame_title.setFont(font)
        self.frame_title.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_title.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_title.setObjectName("frame_title")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_title)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_title = QtWidgets.QLabel(self.frame_title)
        self.label_title.setMinimumSize(QtCore.QSize(0, 40))
        self.label_title.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.label_title.setFont(font)
        self.label_title.setStyleSheet("color: rgb(39, 39, 39);")
        self.label_title.setAlignment(QtCore.Qt.AlignCenter)
        self.label_title.setObjectName("label_title")
        self.verticalLayout_2.addWidget(self.label_title)
        self.horizontalLayout.addWidget(self.frame_title)
        self.frame_btns_right = QtWidgets.QFrame(self.title_bar)
        self.frame_btns_right.setMinimumSize(QtCore.QSize(120, 40))
        self.frame_btns_right.setMaximumSize(QtCore.QSize(120, 40))
        self.frame_btns_right.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_btns_right.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_btns_right.setObjectName("frame_btns_right")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.frame_btns_right)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.btn_minimize = QtWidgets.QPushButton(self.frame_btns_right)
        self.btn_minimize.setMinimumSize(QtCore.QSize(40, 40))
        self.btn_minimize.setMaximumSize(QtCore.QSize(40, 40))
        self.btn_minimize.setToolTip("")
        self.btn_minimize.setStyleSheet(
            "QPushButton {\n"
            "    border: none;\n"
            "}\n"
            "QPushButton:hover {\n"
            "    background-color: rgb(99, 135, 202);\n"
            "}")
        self.btn_minimize.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/minimize_image/Icons/minimize.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_minimize.setIcon(icon3)
        self.btn_minimize.setIconSize(QtCore.QSize(25, 25))
        self.btn_minimize.setObjectName("btn_minimize")
        self.horizontalLayout_3.addWidget(self.btn_minimize)
        self.btn_maximize = QtWidgets.QPushButton(self.frame_btns_right)
        self.btn_maximize.setMinimumSize(QtCore.QSize(40, 40))
        self.btn_maximize.setMaximumSize(QtCore.QSize(40, 40))
        self.btn_maximize.setToolTip("")
        self.btn_maximize.setStyleSheet(
            "QPushButton {\n"
            "    border: none;\n"
            "}\n"
            "QPushButton:hover {\n"
            "    background-color: rgb(99, 135, 202);\n"
            "}")
        self.btn_maximize.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/maximize_image/Icons/maximize.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_maximize.setIcon(icon4)
        self.btn_maximize.setIconSize(QtCore.QSize(25, 25))
        self.btn_maximize.setObjectName("btn_maximize")
        self.horizontalLayout_3.addWidget(self.btn_maximize)
        self.btn_close = QtWidgets.QPushButton(self.frame_btns_right)
        self.btn_close.setMinimumSize(QtCore.QSize(40, 40))
        self.btn_close.setMaximumSize(QtCore.QSize(40, 40))
        self.btn_close.setToolTip("")
        self.btn_close.setStyleSheet(
            "QPushButton {\n"
            "    border: none;\n"
            "}\n"
            "QPushButton:hover {\n"
            "    background-color: rgb(220, 63, 66);\n"
            "}")
        self.btn_close.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/close_image/Icons/close.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_close.setIcon(icon5)
        self.btn_close.setIconSize(QtCore.QSize(25, 25))
        self.btn_close.setObjectName("btn_close")
        self.horizontalLayout_3.addWidget(self.btn_close)
        self.horizontalLayout.addWidget(self.frame_btns_right)
        self.background_layout.addWidget(self.title_bar)
        self.toolbar_bar = QtWidgets.QFrame(self.background_frame)
        self.toolbar_bar.setMaximumSize(QtCore.QSize(16777215, 54))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.toolbar_bar.setFont(font)
        self.toolbar_bar.setStyleSheet("background-color: none;")
        self.toolbar_bar.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.toolbar_bar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.toolbar_bar.setObjectName("toolbar_bar")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.toolbar_bar)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.search_n_sorting_frame = QtWidgets.QFrame(self.toolbar_bar)
        self.search_n_sorting_frame.setMinimumSize(QtCore.QSize(0, 54))
        self.search_n_sorting_frame.setMaximumSize(QtCore.QSize(16777215, 54))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.search_n_sorting_frame.setFont(font)
        self.search_n_sorting_frame.setStyleSheet(
            "QComboBox QAbstractItemView {\n"
            "    color: rgb(39, 39, 39);\n"
            "    border: none;\n"
            "    background-color: rgb(237, 237, 237);\n"
            "    selection-color: rgb(39, 39, 39);\n"
            "    selection-background-color: rgb(212, 212, 212);\n"
            "}")
        self.search_n_sorting_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.search_n_sorting_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.search_n_sorting_frame.setObjectName("search_n_sorting_frame")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.search_n_sorting_frame)
        self.horizontalLayout_5.setContentsMargins(32, 0, 2, 0)
        self.horizontalLayout_5.setSpacing(23)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.search_line = QtWidgets.QLineEdit(self.search_n_sorting_frame)
        self.search_line.textChanged.connect(self.textChangedLogic)
        self.search_line.setMinimumSize(QtCore.QSize(0, 38))
        self.search_line.setMaximumSize(QtCore.QSize(16777215, 38))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.search_line.setFont(font)
        self.search_line.setStyleSheet(
            "QLineEdit {\n"
            "    padding: 5px;\n"
            "    color: rgb(39, 39, 39);\n"
            "    border: 1px solid rgb(212, 212, 212);\n"
            "    background-image: url(:/search_image/Icons/search.svg);\n"
            "    background-position: top right;\n"
            "    background-repeat: no-repeat;\n"
            "    background-origin: content;\n"
            "}\n"
            "\n"
            "QLineEdit:hover {\n"
            "    border: 1px solid rgb(111, 111, 111);\n"
            "}\n"
            "\n"
            "QLineEdit:focus {\n"
            "    border: 1px solid rgb(111, 111, 111);\n"
            "}")
        self.search_line.setObjectName("search_line")
        self.horizontalLayout_5.addWidget(self.search_line)
        self.sorting_combo_box = QtWidgets.QComboBox(self.search_n_sorting_frame)
        self.sorting_combo_box.setMinimumSize(QtCore.QSize(225, 38))
        self.sorting_combo_box.setMaximumSize(QtCore.QSize(225, 38))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(12)
        self.sorting_combo_box.setFont(font)
        self.sorting_combo_box.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.sorting_combo_box.setStyleSheet(
            "QComboBox {\n"
            "    border: none;\n"
            "    background-color: rgb(249, 249, 249);\n"
            "    color: rgb(39, 39, 39);\n"
            "}\n"
            "\n"
            "QComboBox:hover {\n"
            "    background-color: rgb(237, 237, 237);\n"
            "    color: rgb(39, 39, 39);\n"
            "}\n"
            "\n"
            "QComboBox::drop-down {\n"
            "    subcontrol-origin: padding;\n"
            "    subcontrol-position: top right;\n"
            "    width: 38px;\n"
            "    border: none;\n"
            "}\n"
            "\n"
            "QComboBox::down-arrow {\n"
            "    image: url(:/down_arrow_image/Icons/downarrow.svg);\n"
            "    width: 40px;\n"
            "}")
        self.sorting_combo_box.setObjectName("sorting_combo_box")
        self.sorting_combo_box.addItem("")
        self.sorting_combo_box.addItem("")
        self.sorting_combo_box.addItem("")
        self.sorting_combo_box.addItem("")
        self.horizontalLayout_5.addWidget(self.sorting_combo_box)
        self.horizontalLayout_4.addWidget(self.search_n_sorting_frame)
        self.btn_tags = QtWidgets.QPushButton(self.toolbar_bar)
        self.btn_tags.setMinimumSize(QtCore.QSize(38, 38))
        self.btn_tags.setMaximumSize(QtCore.QSize(38, 38))
        self.btn_tags.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_tags.setStyleSheet(
            "QPushButton {\n"
            "    qproperty-icon: url(\" \"); \n"
            "    qproperty-iconSize: 25px 25px;\n"
            "    background-image: url(:/left_arrow_image/Icons/leftarrow.svg);\n"
            "    background-repeat: no-repeat;\n"
            "    border: none;\n"
            "    background-color: rgb(249, 249, 249);\n"
            "    background-position: right;\n"
            "}")
        self.btn_tags.setText("")
        self.btn_tags.setIconSize(QtCore.QSize(25, 25))
        self.btn_tags.setObjectName("btn_tags")
        self.horizontalLayout_4.addWidget(self.btn_tags)
        self.background_layout.addWidget(self.toolbar_bar)
        self.content_bar = QtWidgets.QFrame(self.background_frame)
        self.content_bar.setStyleSheet("background-color: none;")
        self.content_bar.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.content_bar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.content_bar.setObjectName("content_bar")
        self.background_layout.addWidget(self.content_bar)
        self.bottom_bar = QtWidgets.QFrame(self.background_frame)
        self.bottom_bar.setMinimumSize(QtCore.QSize(0, 30))
        self.bottom_bar.setMaximumSize(QtCore.QSize(16777215, 30))
        self.bottom_bar.setStyleSheet("background-color: none;")
        self.bottom_bar.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.bottom_bar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.bottom_bar.setObjectName("bottom_bar")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.bottom_bar)
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.credits_bar = QtWidgets.QFrame(self.bottom_bar)
        self.credits_bar.setMinimumSize(QtCore.QSize(0, 30))
        self.credits_bar.setMaximumSize(QtCore.QSize(16777215, 30))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        self.credits_bar.setFont(font)
        self.credits_bar.setStyleSheet("background-color: none;")
        self.credits_bar.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.credits_bar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.credits_bar.setObjectName("credits_bar")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.credits_bar)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_6.addWidget(self.credits_bar)
        self.frame_grip = QtWidgets.QFrame(self.bottom_bar)
        self.frame_grip.setMinimumSize(QtCore.QSize(30, 30))
        self.frame_grip.setMaximumSize(QtCore.QSize(30, 30))
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        self.frame_grip.setFont(font)
        self.frame_grip.setStyleSheet("background-color: none;\n"
                                      "padding: 5px;")
        self.frame_grip.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_grip.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_grip.setObjectName("frame_grip")
        self.horizontalLayout_6.addWidget(self.frame_grip)
        self.background_layout.addWidget(self.bottom_bar)
        self.backgroung_layout.addWidget(self.background_frame)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Your Meme Database"))
        self.label_title.setText(_translate("MainWindow", "Your Meme Database"))
        self.search_line.setPlaceholderText(_translate("MainWindow", "Поиск по описанию"))
        self.sorting_combo_box.setItemText(0, _translate("MainWindow", "Новые"))
        self.sorting_combo_box.setItemText(1, _translate("MainWindow", "Старые"))
        self.sorting_combo_box.setItemText(2, _translate("MainWindow", "Популярные"))
        self.sorting_combo_box.setItemText(3, _translate("MainWindow", "Непопулярные"))

    def textChangedLogic(self, search_for):
        self.mainwindow.search_filter = search_for
        self.mainwindow.reinit()
        if (ui_settings.settings_data["searchcopybest"] and search_for and
                self.mainwindow.container):
            QtWidgets.QApplication.clipboard().setPixmap(self.mainwindow.container[0].pixmap)
            # print(self.mainwindow.container[0].image)


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
