import sys
sys.path.append('/path/to/meme_database')
from main import *
from PyQt5 import QtWidgets
from PyQt5.Qt import *
from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui


GLOBAL_STATE = 0


class UIFunctions(MainWindow):
    def maximize_restore(self):
        global GLOBAL_STATE
        status = GLOBAL_STATE

        if status == 0:
            self.showMaximized()
            GLOBAL_STATE = 1
            self.ui.backgroung_layout.setContentsMargins(0, 0, 0, 0)
            self.ui.background_frame.setStyleSheet("background-color: rgb(249, 249, 249);")

            icon_restore = QtGui.QIcon()
            icon_restore.addPixmap(QtGui.QPixmap(":/restore_image/Icons/restore.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.ui.btn_maximize.setIcon(icon_restore)
            self.ui.btn_maximize.setIconSize(QtCore.QSize(25, 25))
        else:
            GLOBAL_STATE = 0
            self.showNormal()
            self.resize(self.width(), self.height())
            self.ui.background_layout.setContentsMargins(0, 0, 0, 0)
            self.ui.background_frame.setStyleSheet("background-color: rgb(249, 249, 249);")

            icon_maximize = QtGui.QIcon()
            icon_maximize.addPixmap(QtGui.QPixmap(":/maximize_image/Icons/maximize.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.ui.btn_maximize.setIcon(icon_maximize)
            self.ui.btn_maximize.setIconSize(QtCore.QSize(25, 25))

    def uiDefinitions(self):
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        self.ui.sorting_combo_box.findChild(QFrame).setWindowFlags(Qt.Popup | Qt.NoDropShadowWindowHint)

        self.ui.btn_maximize.clicked.connect(lambda: UIFunctions.maximize_restore(self))

        self.ui.btn_minimize.clicked.connect(lambda: self.showMinimized())

        self.ui.btn_close.clicked.connect(lambda: self.close())

        self.sizegrip = QSizeGrip(self.ui.frame_grip)
        self.sizegrip.setStyleSheet("QSizeGrip { width: 10px; height: 10px; margin: 5px }")

    def returnStatus():
        return GLOBAL_STATE
