"""Управление базой данных"""
import sqlite3
import datetime
import logs


meme_list = list()
meme_tags = dict()
meme_desc = dict()

conn = sqlite3.connect("./Database/MDatabase.db")
cursor = conn.cursor()


def cleanse_db():
    valid_tags = []
    invalid_tags = set()

    cursor.execute("""SELECT id FROM tags""")
    for tag_id in cursor.fetchall():
        valid_tags.append(tag_id[0])

    for tag_id in cursor.execute("""SELECT tag FROM connect"""):
        if tag_id[0] not in valid_tags:
            invalid_tags.add(tag_id[0])

    for tag_id in invalid_tags:
        cursor.execute("DELETE FROM connect WHERE tag = ?", (tag_id,))

    conn.commit()


def from_db_to_list():
    """Добавление в список всех путей к изображениям из базы данных"""
    for pic in cursor.execute("""SELECT pic FROM memes"""):
        meme_list.append(pic[0])


def from_folder_to_db(name):
    """Добавление в базу данных новых мемов"""
    dt_now = str(datetime.datetime.now())[:19]
    if name not in meme_list:
        cursor.execute("INSERT INTO memes (pic, date) VALUES (?, ?);", (name, dt_now))
        meme_list.append(name)


def update_tags():
    cursor.execute("SELECT id, pic FROM memes")
    meme_ids = cursor.fetchall()

    for meme_id in meme_ids:
        meme_tags[meme_id[1]] = []

        cursor.execute("SELECT tag FROM connect WHERE meme = ?", (meme_id[0],))
        connected_tags = cursor.fetchall()

        for tag_id in connected_tags:
            try:
                cursor.execute("SELECT name FROM tags WHERE id = ?", (tag_id[0],))
                meme_tags[meme_id[1]].append(cursor.fetchone()[0])
            except BaseException as error:
                logs.get_log(error)


def update_desc():
    cursor.execute("SELECT pic, description FROM memes")
    res = cursor.fetchall()

    for data in res:
        meme_desc[str(data[0])] = str(data[1])


cleanse_db()
update_tags()
update_desc()
