import logging
import datetime
import os


def get_log(error):
    date = str(datetime.datetime.now())[:10]

    if not os.path.isdir("./logs/"):
        os.mkdir("./logs/")

    logging.basicConfig(filename=f"logs/{date}_logs.log",
                        level=logging.INFO)
                        
    logging.error(error)
